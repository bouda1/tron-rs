extern crate chrono;
extern crate gl;
extern crate nalgebra as na;
extern crate rand;
extern crate sdl2;
extern crate tobj;

mod buffer;
mod engine;
mod model;
mod motorcycle;
pub mod render_gl;
mod tree;
mod zone;
mod skeleton;

use engine::Engine;
use model::Model;
use model::Model3d;
use motorcycle::{Motorcycle, Physic};
use na::{Isometry3, Perspective3, Point3, Vector3};
use tree::Tree;
use zone::Zone;
//use std::sync::{Arc, Mutex};

fn main() {
    let sdl = sdl2::init().unwrap();
    let video_subsystem = sdl.video().unwrap();

    let gl_attr = video_subsystem.gl_attr();

    gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
    gl_attr.set_context_version(4, 1);

    let window = video_subsystem
        .window("Game", 900, 700)
        .opengl()
        .resizable()
        .build()
        .unwrap();

    let _gl_context = window.gl_create_context().unwrap();
    gl::load_with(|s| video_subsystem.gl_get_proc_address(s) as *const std::os::raw::c_void);

    // set up shader program

    use std::ffi::CString;
    let vert_shader =
        render_gl::Shader::from_vert_source(&CString::new(include_str!("triangle.vert")).unwrap())
            .unwrap();

    let frag_shader =
        render_gl::Shader::from_frag_source(&CString::new(include_str!("triangle.frag")).unwrap())
            .unwrap();

    let shader_program = render_gl::Program::from_shaders(&[vert_shader, frag_shader]).unwrap();

    let mut motorcycle = Motorcycle::new();

    let model = Model::new("zones/zone2.obj");
    let mut zone = Zone::new(model);

    // set up vertex array object

    let mut vao: gl::types::GLuint = 0;
    unsafe {
        gl::GenVertexArrays(1, &mut vao);
    }

    // set up shared state for window

    unsafe {
        gl::Viewport(0, 0, 900, 700);
        gl::ClearColor(0.3, 0.3, 0.5, 1.0);
    }

    let mut event_pump = sdl.event_pump().unwrap();

    // Our object is translated along the x axis.
    let model = Isometry3::new(Vector3::x(), na::zero());

    // Our camera looks toward the point (1.0, 0.0, 0.0).
    // It is located at (0.0, 0.0, 1.0).
    let target = Point3::new(0_f32, -13_f32, 0.0);

    unsafe {
        // Enable depth test
        gl::Enable(gl::DEPTH_TEST);

        // Accept fragment if it is closer to the camera than the former one
        gl::DepthFunc(gl::LESS);

        // Cull triangles which normal is not towards the camera
        gl::Enable(gl::CULL_FACE);
    }

    // main loop

    let mut angle = 0.0_f32;

    let handle = Engine::start_loop(&zone, &mut motorcycle);

    'main: loop {
        let eye = Point3::new(5.0_f32 * angle.cos(), -13_f32, 5.0_f32 * angle.sin());
        angle += 0.01;

        let view = Isometry3::look_at_rh(&eye, &target, &Vector3::y());

        // A perspective projection.
        let projection = Perspective3::new(90.0 / 70.0, std::f32::consts::PI / 2.0, 1.0, 1000.0);

        // The combination of the model with the view is still an isometry.
        let model_view = view * model;

        // Convert everything to a `Matrix4` so that they can be combined.
        let mat_model_view = model_view.to_homogeneous();

        // Combine everything.
        let model_view_projection = projection.as_matrix() * mat_model_view;

        for event in event_pump.poll_iter() {
            match event {
                sdl2::event::Event::Quit { .. } => break 'main,
                _ => {}
            }
        }

        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
        }

        // draw triangle

        // Get a handle for our "MVP" uniform
        // Only during the initialisation
        unsafe {
            let program_id: gl::types::GLuint = shader_program.id();

            let mvp_name = CString::new("MVP").unwrap();
            let matrix_id = gl::GetUniformLocation(program_id, mvp_name.as_ptr());
            // Send our transformation to the currently bound shader, in the "MVP" uniform
            // This is done in the main loop since each model will have a different MVP matrix (At least for the M part)
            let v = model_view_projection.as_slice();
            gl::UniformMatrix4fv(matrix_id, 1, gl::FALSE, v.as_ptr());
        }

        shader_program.set_used();
        unsafe {
            gl::BindVertexArray(vao);
            let program_id: gl::types::GLuint = shader_program.id();

            let mvp_name = CString::new("MVP").unwrap();
            let matrix_id = gl::GetUniformLocation(program_id, mvp_name.as_ptr());

            zone.draw();

            let t = motorcycle.get_transform();
            let v = model_view_projection * t;
            let v = v.as_slice();
            gl::UniformMatrix4fv(matrix_id, 1, gl::FALSE, v.as_ptr());
            motorcycle.draw();

            gl::BindVertexArray(0);
        }

        window.gl_swap_window();
    }
    handle.join().unwrap();
}