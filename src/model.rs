use buffer::{ArrayBuffer, ElementArrayBuffer};
use rand::Rng;
use std::path::Path;

pub trait Model3d {
    fn get_model(&self) -> &Model;

    fn draw(&self) {
        self.get_model().draw();
    }
}

pub struct Model {
    pub vertices: Vec<f32>,
    pub normals: Vec<f32>,
    pub indices: Vec<u16>,
    pub colors: Vec<f32>,
    pub vertex_buffer: ArrayBuffer,
    pub normal_buffer: ArrayBuffer,
    pub color_buffer: ArrayBuffer,
    pub element_buffer: ElementArrayBuffer,
}

impl Model {
    pub fn new(name: &str) -> Model {
        let mut fullname: String = "objects/".to_owned();
        fullname.push_str(name);

        // set up vertex buffer object
        let mut vertices: Vec<f32> = Vec::new();
        let mut normals: Vec<f32> = Vec::new();
        let mut indices: Vec<u16> = Vec::new();
        let mut colors: Vec<f32> = Vec::new();

        {
            let model_obj = tobj::load_obj(&Path::new(&fullname));
            assert!(model_obj.is_ok());
            let (models, _materials) = model_obj.unwrap();
            let mut size: u16 = 0;

            let mut rng = rand::thread_rng();

            for m in models.iter() {
                let mesh = &m.mesh;
                for v in mesh.positions.iter() {
                    vertices.push(*v);
                    colors.push(rng.gen::<f32>());
                }
                for n in mesh.normals.iter() {
                    normals.push(*n);
                }
                for i in mesh.indices.iter() {
                    indices.push(size + *i as u16);
                }
                size = (vertices.len() / 3) as u16;
            }
        }
        let vertex_buffer = Model::create_array_buffer(&vertices);
        let normal_buffer = Model::create_array_buffer(&normals);
        let color_buffer = Model::create_array_buffer(&colors);
        let element_buffer = Model::create_element_array_buffer(&indices);
        Model {
            vertices,
            normals,
            indices,
            colors,
            vertex_buffer,
            normal_buffer,
            color_buffer,
            element_buffer,
        }
    }

    pub fn update_color_buffer(&mut self) {
        self.color_buffer = Model::create_array_buffer(&self.colors);
    }

    fn create_array_buffer(vec: &[f32]) -> ArrayBuffer {
        let retval = ArrayBuffer::new();
        retval.bind();
        retval.static_draw_data(vec);
        retval.unbind();
        retval
    }

    pub fn create_element_array_buffer(vec: &[u16]) -> ElementArrayBuffer {
        let retval = ElementArrayBuffer::new();
        retval.bind();
        retval.static_draw_data(&vec);
        retval.unbind();
        retval
    }

    pub fn draw(&self) {
        unsafe {
            self.vertex_buffer.bind();
            gl::EnableVertexAttribArray(0); // this is "layout (location = 0)" in vertex shader
            gl::VertexAttribPointer(
                0,                // index of the generic vertex attribute ("layout (location = 0)")
                3,                // the number of components per generic vertex attribute
                gl::FLOAT,        // data type
                gl::FALSE,        // normalized (int-to-float conversion)
                0,                // stride (byte offset between consecutive attributes)
                std::ptr::null(), // offset of the first component
            );
            self.vertex_buffer.unbind();

            self.color_buffer.bind();
            gl::EnableVertexAttribArray(1); // this is "layout (location = 0)" in vertex shader
            gl::VertexAttribPointer(
                1,                // index of the generic vertex attribute ("layout (location = 0)")
                3,                // the number of components per generic vertex attribute
                gl::FLOAT,        // data type
                gl::FALSE,        // normalized (int-to-float conversion)
                0,                // stride (byte offset between consecutive attributes)
                std::ptr::null(), // offset of the first component
            );
            self.color_buffer.unbind();

            self.normal_buffer.bind();
            gl::EnableVertexAttribArray(2); // this is "layout (location = 0)" in vertex shader
            gl::VertexAttribPointer(
                2,                // index of the generic vertex attribute ("layout (location = 0)")
                3,                // the number of components per generic vertex attribute
                gl::FLOAT,        // data type
                gl::FALSE,        // normalized (int-to-float conversion)
                0,                // stride (byte offset between consecutive attributes)
                std::ptr::null(), // offset of the first component
            );
            self.normal_buffer.unbind();

            self.element_buffer.bind();
            gl::DrawElements(
                gl::TRIANGLES, // mode
                self.indices.len() as i32,
                gl::UNSIGNED_SHORT,
                std::ptr::null(), // offset of the first component
            );
        }
    }
}
