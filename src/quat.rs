use std::ops::{Add, Mul, Sub};

#[derive(Debug, PartialEq, Clone, Copy)]
struct Quat {
    x: f32,
    y: f32,
    z: f32,
    w: f32,
}

impl Add for Quat {
    type Output = Quat;

    fn add(self, other: Quat) -> Quat {
        Quat {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
            w: self.w + other.w,
        }
    }
}

impl Sub for Quat {
    type Output = Quat;

    fn sub(self, other: Quat) -> Quat {
        Quat {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
            w: self.w - other.w,
        }
    }
}

impl Mul for Quat {
    type Output = Quat;

    fn mul(self, other: Quat) -> Quat {
        Quat {
            w: self.w * other.w - self.x * other.x - self.y * other.y - self.z * other.z,
            x: self.w * other.x + self.x * other.w + self.y * other.z - self.z * other.y,
            y: self.w * other.y + self.y * other.w - self.x * other.z + self.z * other.x,
            z: self.w * other.z + self.z * other.w + self.x * other.y - self.y * other.x,
        }
    }
}

impl Quat {
    pub fn norm_sqr(&self) -> f32 {
        self.w * self.w + self.x * self.x + self.y * self.y + self.z * self.z
    }

    pub fn norm(&self) -> f32 {
        self.norm_sqr().sqrt()
    }

    pub fn inv(&self) -> Quat {
        let n = self.norm_sqr();
        Quat {
            w: self.w / n,
            x: -self.x / n,
            y: -self.y / n,
            z: -self.z / n,
        }
    }

    pub fn conj(&self) -> Quat {
        Quat {
            w: self.w,
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }

    pub fn normalize(&mut self) {
        let n = self.norm();
        self.x /= n;
        self.y /= n;
        self.z /= n;
        self.w /= n;
    }

    pub fn arg(&mut self) -> f32 {
        self.w.acos() as f32
    }
}

#[cfg(test)]
mod tests {
    use quat::*;

    #[test]
    fn equality() {
        let a = Quat {
            x: 1.0,
            y: 2.0,
            z: 3.0,
            w: 4.0,
        };
        assert_eq!(a, a);
    }

    #[test]
    fn add() {
        let a = Quat {
            x: 1.0,
            y: 2.0,
            z: 3.0,
            w: 4.0,
        };
        let b = Quat {
            x: 4.0,
            y: 3.0,
            z: 2.0,
            w: 1.0,
        };
        let sum = Quat {
            x: 5.0,
            y: 5.0,
            z: 5.0,
            w: 5.0,
        };
        assert_eq!(a + b, sum);
    }

    #[test]
    fn mul1() {
        let a = Quat {
            w: 0.0,
            x: 1.0,
            y: 0.0,
            z: 0.0,
        };
        let b = Quat {
            w: 0.0,
            x: 0.0,
            y: 1.0,
            z: 0.0,
        };
        let prod = Quat {
            w: 0.0,
            x: 0.0,
            y: 0.0,
            z: 1.0,
        };
        assert_eq!(a * b, prod);
    }

    #[test]
    fn mul2() {
        let a = Quat {
            w: 0.0,
            x: 1.0,
            y: 0.0,
            z: 0.0,
        };
        let b = Quat {
            w: 0.0,
            x: 0.0,
            y: 1.0,
            z: 0.0,
        };
        let prod = Quat {
            w: 0.0,
            x: 0.0,
            y: 0.0,
            z: -1.0,
        };
        assert_eq!(b * a, prod);
    }

    #[test]
    fn mul3() {
        let a = Quat {
            w: 1.0,
            x: 0.0,
            y: 0.0,
            z: 0.0,
        };
        let b = Quat {
            w: 0.0,
            x: 1.0,
            y: 2.0,
            z: 3.0,
        };
        assert_eq!(a * b, b);
        assert_eq!(b * a, b);
    }

    #[test]
    fn inv() {
        let a = Quat {
            w: 13.0,
            x: 7.0,
            y: 11.0,
            z: 4.0,
        };
        let b = a.inv();
        let eps = (a * b - Quat {
            w: 1.0,
            x: 0.0,
            y: 0.0,
            z: 0.0,
        }).norm();
        assert!(eps.abs() < 0.00001);
    }

    #[test]
    fn normalize() {
        let mut a = Quat {
            w: 2.0,
            x: 4.0,
            y: 8.0,
            z: 10.0,
        };
        a.normalize();
        assert!((a.norm() - 1.0).abs() < 0.0001);
    }
}
