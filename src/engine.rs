use na::{Point3, Vector3};
use Model;
use Model3d;
use Motorcycle;
use Physic;
use Zone;
use Tree;
use std::vec::Vec;
use std::thread;
use std::time::Duration;


pub struct Engine {
    indices: Vec<usize>,
    vertices: Vec<Point3<f32>>,
    normals: Vec<Vector3<f32>>,
    tree: Tree,
}

impl Engine {
    
    /// Engine's Constructor. It is constructed upon the zone model.
    /// Later the scheduler will check interaction between this zone and motorcycles.
    ///
    /// # Arguments
    ///
    /// * `model` - A zone model.
    ///
    fn new(model: &Model) -> Engine {
        let mut indices = Vec::new();
        for k in 0..model.indices.len() {
            indices.push((model.indices[k]) as usize);
        }

        let mut vertices = Vec::new();
        for k in (0..model.vertices.len()).step_by(3) {
            let v = Point3::new(model.vertices[k], model.vertices[k + 1], model.vertices[k + 2]);
            vertices.push(v);
        }
        let mut normals = Vec::new();
        for k in (0..model.normals.len()).step_by(3) {
            let v = Vector3::new(model.vertices[k], model.vertices[k + 1], model.vertices[k + 2]);
            normals.push(v);
        }
        let tree = Tree::build_tree(&indices, &vertices);

        Engine {
            indices,
            vertices,
            normals,
            tree,
        }
    }

    pub fn start_loop(zone: &Zone, motorcycle: &mut Motorcycle) -> std::thread::JoinHandle<()> {
        let engine = Engine::new(zone.get_model());
        let physic_r = motorcycle.physic.clone();
        let physic_w = motorcycle.physic.clone();

        let handle = thread::spawn(move|| {
            //let p = physic_r.read().unwrap();
            loop {
                let bb = match physic_r.read() {
                    Ok(p) => p.get_next_bounding_box(),
                    Err(_) => unreachable!(),
                };

                // Let's get the bounding box after the future move
                let idx = engine.to_intersect(&bb);
                
                // Not functional, useful to debug
                //zone.select(&idx);
                let mut no_intersection = idx.is_empty();
                if !no_intersection {
                    no_intersection = match physic_r.read() {
                        Ok(p) => !engine.check_intersection(&idx, &p),
                        Err(_) => unreachable!(),
                    };
                }
                if no_intersection {
                    if let Ok(mut physic) = physic_w.write() {
                        physic.apply_forces();
                        physic.mv();
                    }
                }
                std::thread::sleep(Duration::from_millis(10));
            }
        });

        handle
    }

    fn to_intersect(&self, bb: &(Vector3<f32>, Vector3<f32>)) -> Vec<usize> {
        let mut retval = Vec::new();
        let min = (bb.0.x - 1_f32, bb.0.y - 1_f32, bb.0.z - 1_f32);
        let max = (bb.1.x + 1_f32, bb.1.y + 1_f32, bb.1.z + 1_f32);
        for val in self.tree.range(&min, &max) {
            let idx: [usize; 3] = [
                // Index of the first vertex
                self.indices[3 * val as usize] as usize,
                // Index of the second vertex
                self.indices[(3 * val + 1) as usize] as usize,
                // Index of the third vertex
                self.indices[(3 * val + 2) as usize] as usize,
            ];

            let mut min = self.vertices[idx[0]];
            let mut max = self.vertices[idx[0]];
            for it in idx.iter().skip(1) {
                if self.vertices[*it].x < min.x {
                    min.x = self.vertices[*it].x;
                } else if self.vertices[*it].x > max.x {
                    max.x = self.vertices[*it].x;
                }

                if self.vertices[*it + 1].y < min.y {
                    min.y = self.vertices[*it + 1].y;
                } else if self.vertices[*it + 1].y > max.y {
                    max.y = self.vertices[*it + 1].y;
                }

                if self.vertices[*it + 2].z < min.z {
                    min.z = self.vertices[*it + 2].z;
                } else if self.vertices[*it + 2].z > max.z {
                    max.z = self.vertices[*it + 2].z;
                }
            }
            if bb.1.x >= min.x
                && max.x >= bb.0.x
                && bb.1.y >= min.y
                && max.y >= bb.0.y
                && bb.1.z >= min.z
                && max.z >= bb.0.z
            {
                retval.push(val)
            }
        }
        retval
    }

    /// Returns a boolean telling if the motorcycle will collide the zone.
    ///
    /// # Arguments
    ///
    /// * `idx` - An array of `u16` representing vertices indices of the zone.
    /// * `motorcycle` - The motorcycle to check intersection with.
    pub fn check_intersection(&self, idx: &[usize], p: &Physic) -> bool {
        let frh = p.get_frh();
        let frh1 = p.get_contacts(&frh.0, &frh.1, &frh.2);
        let frh2 = p.get_next_contacts(&frh.0, &frh.1, &frh.2);
        let mut retval = false;
        let inters = self.get_intersection(&frh1.0, &frh2.0, &idx);
        if let Some(_x) = inters {
            retval = true
        }
        retval
    }

    fn get_intersection(
        &self,
        p0: &Point3<f32>,
        p1: &Point3<f32>,
        idx: &[usize],
    ) -> Option<Point3<f32>> {
        for i in idx {
            let ind1: usize = self.indices[3 * *i];
            let ind2: usize = self.indices[3 * *i + 1];
            let ind3: usize = self.indices[3 * *i + 2];
            let uu: Vector3<f32> = self.vertices[ind1] - p0;

            let normal = (self.normals[ind1] + self.normals[ind2] + self.normals[ind3]) / 3_f32;
            let vv: Vector3<f32> = p1 - p0;

            let den = normal.dot(&vv);
            if den < -1e-7 || den > 1e-8 {
                let r = normal.dot(&uu) / den;

                if r >= 0_f32 && r <= 1_f32 {
                    let u = self.vertices[ind2] - self.vertices[ind1];
                    let v = self.vertices[ind3] - self.vertices[ind1];
                    let pp: Point3<f32> = p0 + (p1 - p0) * r;
                    let w = pp - self.vertices[ind1];
                    let scal_uv = u.dot(&v);
                    let scal_wv = w.dot(&v);
                    let scal_wu = w.dot(&u);
                    let scal_uu = u.dot(&u);
                    let scal_vv = v.dot(&v);

                    let den = scal_uv * scal_uv - scal_uu * scal_vv;
                    if den < -1e-7 || den > 1e-7 {
                        let coeff_v = (scal_uv * scal_wv - scal_vv * scal_wu) / den;
                        if coeff_v >= 0_f32 && coeff_v <= 1_f32 {
                            let coeff_u = (scal_uv * scal_wu - scal_uu * scal_wv) / den;
                            if coeff_u >= 0_f32 && coeff_u <= 1_f32 {
                                return Some(pp);
                            }
                        }
                    }
                }
            }
        }
        None
    }
}
