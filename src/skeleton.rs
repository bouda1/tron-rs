use na::Point3;

#[derive(Debug)]
pub struct Skeleton {
    // Contact point on the floor of the front wheel
    pub fw: Point3<f32>,    
    // Contact point on the floor of the rear wheel
    pub rw: Point3<f32>,
    // Top of the head
    pub head: Point3<f32>,
}

impl Skeleton {
    pub fn motorcycle() -> Skeleton {
        let fw = Point3::new(1.4_f32, -1.1_f32, 0_f32);
        let rw = Point3::new(-2.7_f32, -1.1_f32, 0_f32);
        let head = Point3::new(0_f32, 1_f32, 0_f32);

        Skeleton {
            fw,
            rw,
            head,
        }
    }
}