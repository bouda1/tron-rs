use model::{Model, Model3d};
use na::{Isometry3, Matrix4, Point3, Translation3, Unit, UnitQuaternion, Vector3};
use skeleton::Skeleton;
use std::sync::{Arc, RwLock};

#[derive(Debug)]
pub struct Physic {
    // The isometry applied on the motorcycle (translation + rotation)
    // Rotation + Translation
    pub iso: Isometry3<f32>,
    // speed of G
    pub vg: Vector3<f32>,
    pub skeleton: Skeleton,

    // G in the motorcycle coordinates
    pub g: Point3<f32>,

    // Force applied on fw
    ffw: Vector3<f32>,

    // Force applied on rw
    frw: Vector3<f32>,
}

pub struct Motorcycle {
    // The graphical model
    pub model: Model,

    // Test for concurrent access
    pub physic: Arc<RwLock<Physic>>,
}

impl Model3d for Motorcycle {
    fn get_model(&self) -> &Model {
        &self.model
    }
}

impl Physic {
    fn _get_bounding_box(
        fw: &Point3<f32>,
        rw: &Point3<f32>,
        head: &Point3<f32>,
    ) -> (Vector3<f32>, Vector3<f32>) {
        let (mut x1, mut y1, mut z1) = (rw.x, rw.y, rw.z);
        let (mut x2, mut y2, mut z2) = (rw.x, rw.y, rw.z);
        if fw.x < x1 {
            x1 = fw.x;
        } else if fw.x > x2 {
            x2 = fw.x;
        }
        if fw.y < y1 {
            y1 = fw.y;
        } else if fw.y > y2 {
            y2 = fw.y;
        }
        if fw.z < z1 {
            z1 = fw.z;
        } else if fw.z > z2 {
            z2 = fw.z;
        }

        if head.x < x1 {
            x1 = head.x;
        } else if head.x > x2 {
            x2 = head.x;
        }
        if head.y < y1 {
            y1 = head.y;
        } else if head.y > y2 {
            y2 = head.y;
        }
        if head.z < z1 {
            z1 = head.z;
        } else if head.z > z2 {
            z2 = head.z;
        }

        (Vector3::new(x1, y1, z1), Vector3::new(x2, y2, z2))
    }

    pub fn get_next_bounding_box(&self) -> (Vector3<f32>, Vector3<f32>) {
        let (fw, rw, head) = self.get_next_contacts(&self.skeleton.fw, &self.skeleton.rw, &self.skeleton.head);

        // Now the three previous vectors are at the future position
        Physic::_get_bounding_box(&fw, &rw, &head)
    }

    pub fn get_next_contacts(
        &self,
        p1: &Point3<f32>,
        p2: &Point3<f32>,
        p3: &Point3<f32>,
    ) -> (Point3<f32>, Point3<f32>, Point3<f32>) {
        let (fw, rw, head) = self.get_contacts(&p1, &p2, &p3);
        // We add to it the forces resultant
        let vg = self.vg + self.get_forces();

        // We determine the isometry to apply to the contacts to have the future position
        let mv = Translation3::from(vg);
        (mv * fw, mv * rw, mv * head)
    }

    pub fn get_contacts(
        &self,
        p1: &Point3<f32>,
        p2: &Point3<f32>,
        p3: &Point3<f32>,
    ) -> (Point3<f32>, Point3<f32>, Point3<f32>) {
        (self.iso * p1, self.iso * p2, self.iso * p3)
    }

    pub fn get_forces(&self) -> Vector3<f32> {
        // Forces are in the zone coordinates
        let mut gravity = Vector3::new(0_f32, -0.01_f32, 0_f32);
        let rot = self.iso.rotation;
        gravity = rot * gravity;
        gravity
    }

    pub fn apply_forces(&mut self) {
        self.vg += self.get_forces();
    }

    pub fn mv(&mut self) {
        let mv = Translation3::from(self.vg);
        self.iso = mv * self.iso;
    }

    pub fn get_frh(&self) -> (Point3<f32>, Point3<f32>, Point3<f32>) {
        (self.skeleton.fw, self.skeleton.rw, self.skeleton.head)
    }

}

impl Motorcycle {
    pub fn new() -> Motorcycle {
        // set up vertex buffer object
        let model = Model::new("motorcycle.obj");
        let skeleton = Skeleton::motorcycle();
        let axis = Unit::new_normalize(Vector3::new(0_f32, 1_f32, 0_f32));
        let rotation = UnitQuaternion::from_axis_angle(&axis, 0_f32);
        let translation = Translation3::new(0_f32, 5_f32, 0_f32);
        let g = Point3::origin();
        let vg = Vector3::zeros();
        let iso = Isometry3::from_parts(translation, rotation);
        let ffw = Vector3::zeros();
        let frw = Vector3::zeros();

        let physic = Physic{iso: iso, vg: vg, skeleton: skeleton, g: g, ffw: ffw, frw: frw};
        let physic = Arc::new(RwLock::new(physic));

        Motorcycle {
            model,
            physic,
        }
    }

    pub fn get_transform(&mut self) -> Matrix4<f32> {
        let p = self.physic.read().unwrap();
        p.iso.to_homogeneous()
    }
}
