use std::ops::{Add, AddAssign, Mul, DivAssign};

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Vertex {
    pub position: (f32, f32, f32),
}

implement_vertex!(Vertex, position);

impl AddAssign for Vertex {
    fn add_assign(&mut self, other: Vertex) {
        self.position.0 += other.position.0;
        self.position.1 += other.position.1;
        self.position.2 += other.position.2;
    }
}

impl DivAssign<f32> for Vertex {
  fn div_assign(&mut self, other: f32) {
    self.position.0 /= other;
    self.position.1 /= other;
    self.position.2 /= other;
  }
}

impl Mul<f32> for Vertex {
  type Output = Vertex;

  fn mul(self, other: f32) -> Vertex {
    Vertex {
      position: (
        self.position.0 * other,
        self.position.1 * other,
        self.position.2 * other,
      ),
    }
  }
}

impl Mul for Vertex {
  type Output = Vertex;

  fn mul(self, other: Vertex) -> Vertex {
    Vertex {
      position: (
        self.position.1 * other.position.2 - self.position.2 * other.position.1,
        self.position.2 * other.position.0 - self.position.0 * other.position.2,
        self.position.0 * other.position.1 - self.position.1 * other.position.0,
      ),
    }
  }
}

impl Add for Vertex {
  type Output = Vertex;

  fn add(self, other: Vertex) -> Vertex {
    Vertex {
      position: (
        self.position.0 + other.position.0,
        self.position.1 + other.position.1,
        self.position.2 + other.position.2,
      ),
    }
  }
}

#[derive(Copy, Clone)]
pub struct Normal {
  pub normal: (f32, f32, f32),
}

implement_vertex!(Normal, normal);
