use model::{Model, Model3d};

pub struct Zone {
    pub model: Model,
}

impl Model3d for Zone {
    fn get_model(&self) -> &Model {
        &self.model
    }
}

impl Zone {
    pub fn new(model: Model) -> Zone {
        // set up vertex buffer object
        Zone { model }
    }

    pub fn select(&mut self, idx: &[usize]) {
        for i in idx {
            let ii1 = self.model.indices[3 * *i] as usize;
            let ii2 = self.model.indices[3 * *i + 1] as usize;
            let ii3 = self.model.indices[3 * *i + 2] as usize;
            self.model.colors[3 * ii1] = 1_f32;
            self.model.colors[3 * ii1 + 1] = 1_f32;
            self.model.colors[3 * ii1 + 2] = 1_f32;
            self.model.colors[3 * ii2] = 1_f32;
            self.model.colors[3 * ii2 + 1] = 1_f32;
            self.model.colors[3 * ii2 + 2] = 1_f32;
            self.model.colors[3 * ii3] = 1_f32;
            self.model.colors[3 * ii3 + 1] = 1_f32;
            self.model.colors[3 * ii3 + 2] = 1_f32;
        }
        self.model.update_color_buffer();
    }
}
