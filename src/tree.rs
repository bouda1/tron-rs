use std::cmp::Ordering;
use std::collections::BTreeMap;
use std::ops::Bound::Included;
use std::vec::Vec;
use na::{Point3, Vector3};

pub struct Key(f32);

impl PartialEq for Key {
    fn eq(&self, other: &Key) -> bool {
        self.0 == other.0
    }
}

impl Eq for Key {}

impl Ord for Key {
    fn cmp(&self, other: &Key) -> Ordering {
        if self.0 < other.0 {
            return Ordering::Less;
        } else if self.0 > other.0 {
            return Ordering::Greater;
        }
        Ordering::Equal
    }
}

impl PartialOrd for Key {
    fn partial_cmp(&self, other: &Key) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub struct Tree {
    tree: BTreeMap<Key, BTreeMap<Key, BTreeMap<Key, usize>>>,
}

impl Tree {
    pub fn build_tree(indices: &[usize], vertices: &[Point3<f32>]) -> Tree {
        let mut v = Point3::origin();
        //let mut v = Vector3::new(0_f32, 0_f32, 0_f32);
        let mut count = 0;
        let mut pos = 0_usize;
        let mut retval = Tree { tree: BTreeMap::new() };
        for i in indices {
            let trans: Vector3<f32> = vertices[*i as usize] - Point3::origin();
            v += trans;
            count += 1;
            if count == 3 {
                // Division by 3 for the average...
                v /= 3_f32;
                retval.insert(v.x, v.y, v.z, pos);
                count = 0;
                v = Point3::origin();
                pos += 1;
            }
        }
        retval
    }

    pub fn insert(&mut self, x: f32, y: f32, z: f32, idx: usize) {
        let xx = Key { 0: x };
        let yy = Key { 0: y };
        let zz = Key { 0: z };
        self.tree
            .entry(xx)
            .or_insert_with(BTreeMap::new)
            .entry(yy)
            .or_insert_with(BTreeMap::new)
            .insert(zz, idx);
    }

    pub fn range(
        &self,
        (x1, y1, z1): &(f32, f32, f32),
        (x2, y2, z2): &(f32, f32, f32),
    ) -> Vec<usize> {
        let mut retval = Vec::new();
        let (xx1, xx2) = (Key { 0: *x1 }, Key { 0: *x2 });
        let (yy1, yy2) = (Key { 0: *y1 }, Key { 0: *y2 });
        let (zz1, zz2) = (Key { 0: *z1 }, Key { 0: *z2 });
        for (_, ref tree_y) in self.tree.range((Included(&xx1), Included(&xx2))) {
            for (_, ref tree_z) in tree_y.range((Included(&yy1), Included(&yy2))) {
                for (_, &val) in tree_z.range((Included(&zz1), Included(&zz2))) {
                    retval.push(val);
                }
            }
        }
        retval
    }

    pub fn len(&self) -> usize {
        let mut retval: usize = 0;
        for (_, ref tree_y) in self.tree.iter() {
            for (_, ref tree_z) in tree_y.iter() {
                retval += tree_z.len();
            }
        }
        retval
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_lt() {
        let a = Key { 0: 12_f32 };
        let b = Key { 0: 18_f32 };

        assert!(a < b);
    }

    #[test]
    fn test_le() {
        let a = Key { 0: 12_f32 };
        let b = Key { 0: 18_f32 };

        assert!(a <= b);
    }

    #[test]
    fn test_eq() {
        let a = Key { 0: 12_f32 };
        let b = Key { 0: 12_f32 };

        assert!(a == b);
    }

    #[test]
    fn test_ge() {
        let a = Key { 0: 12_f32 };
        let b = Key { 0: -12_f32 };

        assert!(a >= b);
    }

    #[test]
    fn test_gt() {
        let a = Key { 0: 12_f32 };
        let b = Key { 0: -12_f32 };

        assert!(a > b);
    }

    #[test]
    fn test_insert() {
        let mut tree = Tree { tree: BTreeMap::new() };
        tree.insert(1_f32, 2_f32, 3_f32, 18);
        assert_eq!(tree.len(), 1);
        tree.insert(2_f32, 1_f32, 5_f32, 19);
        assert_eq!(tree.len(), 2);
        tree.insert(2_f32, 1_f32, 5_f32, 20);
        assert_eq!(tree.len(), 2);
        tree.insert(3_f32, 2_f32, 8_f32, 23);
        assert_eq!(tree.len(), 3);
        assert_eq!(
            tree.range(&(1_f32, 1_f32, 3_f32), &(3_f32, 2_f32, 5_f32))
                .len(),
            2
        );
    }
}
